/**
 * Created on 14.07.17.
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import Api from '@/api';

describe('Api', () => {
  it('Should send a GET request', () => {
    Api.get('http://swapi.co/api/people/1')
      .then(response => expect(response.body.data.name).to.equal('Luke Skywalker'))
      .catch((error) => { throw error; });
  });
  it('Should send a POST request', () => {
    const data = {
      title: 'foo',
      body: 'bar',
      userId: 1,
    };
    Api.post('http://jsonplaceholder.typicode.com/posts')
      .then((response) => {
        expect(response.body.data.title).to.equal(data.title);
        expect(isNaN(parseInt(response.id, 0))).to.equal(false);
      })
      .catch((error) => { throw error; });
  });
});
