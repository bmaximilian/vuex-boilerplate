/**
 * Created on 03.08.17.
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import Greeting from '@/modules/greeter/models/Greeting';

const greet = new Greeting();

describe('Greeting', () => {
  it('Should display text', () => {
    expect(greet.greet).to.equal('Hello');
    expect(greet.object).to.equal('...');
    expect(greet.text).to.equal('Hello...');
  });
  it('Should set a new greet text', () => {
    greet.setGreet('Hi');
    expect(greet.greet).to.equal('Hi');
    expect(greet.object).to.equal('...');
    expect(greet.text).to.equal('Hi...');
  });
  it('Should set a new object text', () => {
    greet.setObject('World');
    expect(greet.greet).to.equal('Hi');
    expect(greet.object).to.equal('World');
    expect(greet.text).to.equal('Hi World');
  });
});
