/**
 * JSON api test server
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

const jsonServer = require('json-server');
const test = require('./test.json');

const server = jsonServer.create();

/* eslint-disable no-console */

server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// GET routes
server.get('/test', (req, res) => {
  console.log('GET: /test');
  res.jsonp(test);
  console.log('Response sent');
});

// Middleware to user POST routes
server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body.createdAt = Date.now();
  }
  // Continue to JSON Server router
  next();
});

// POST routes
server.post('/test', (req, res) => {
  console.log('POST: /test');
  res.jsonp(test);
  console.log('Response sent');
});

// Start the server
server.listen(3000, () => {
  console.log('JSON Server is running on port 3000');
  console.log('Routes:');
  console.log('GET: /test');
  console.log('POST: /test');
});
