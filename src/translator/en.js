/**
 * German language strings
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

export default {
  locale: 'en-US',
  translations: {
    'greeter.greet': 'Hello',
    'greeter.object': 'World',
    'greeter.subtitle': 'A Vuex greet',
    'greeter.linkbutton.de.label': 'German',
    'greeter.linkbutton.en.label': 'English',
  },
};
