/**
 * German language strings
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

export default {
  locale: 'de-DE',
  translations: {
    'greeter.greet': 'Hallo',
    'greeter.object': 'Welt',
    'greeter.subtitle': 'Eine Vuex Begrüßung',
    'greeter.linkbutton.de.label': 'Deutsch',
    'greeter.linkbutton.en.label': 'Englisch',
  },
};
