/**
 * Router of the Vue app
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import Vue from 'vue';
import Router from 'vue-router';
import MainPage from '@/modules/main/components/MainPage';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '*',
      name: 'main-page',
      component: MainPage,
    },
  ],
});
