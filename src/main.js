/**
 * Main entry point of the Vue app
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import vuexI18n from 'vuex-i18n';
import { sync } from 'vuex-router-sync';
import App from './App';
import router from './router';
import store from './store';
import Translator, { languages } from './translator';

Vue.config.productionTip = false;
Vue.config.debug = true;

Vue.use(VueResource);
Vue.use(vuexI18n.plugin, store);
sync(store, router);

for (let i = 0; i < languages.length; i += 1) {
  Vue.i18n.add(languages[i].locale, languages[i].translations);
}
Vue.i18n.set(Translator.getLocale());

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
