/**
 * Default state
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import Greeting from '../../models/Greeting';

export default new Greeting();
