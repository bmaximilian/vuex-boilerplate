/**
 * Actions og greeter
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import Translator from '@/translator';
import * as actionTypes from '../action-types';
import * as mutationTypes from '../mutation-types';

export default {
  /**
   * Action to set the object of the greeter
   * @param commit
   * @param t
   */
  [actionTypes.SET_OBJECT]({ commit }) {
    new Promise((resolve) => {
      setTimeout(() => {
        resolve(Translator.t('greeter.object'));
      }, 1000);
    })
      .then((response) => {
        commit(mutationTypes.SET_OBJECT, response);
      });
  },
  /**
   * Action to set the greet of the greeter
   * @param commit
   * @param t
   */
  [actionTypes.SET_GREET]({ commit }) {
    commit(mutationTypes.SET_GREET, Translator.t('greeter.greet'));
  },
  /**
   * Reloads the text of the greeter
   * @param commit
   */
  [actionTypes.RELOAD]({ commit }) {
    commit(mutationTypes.SET_GREET, Translator.t('greeter.greet'));
    commit(mutationTypes.SET_OBJECT, Translator.t('greeter.object'));
  },
};
