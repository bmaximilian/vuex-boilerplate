/**
 * Mutations of greeter
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import * as mutationTypes from '../mutation-types';

export default {
  /**
   * Mutates the object in the state
   * @param state
   * @param object
   */
  [mutationTypes.SET_OBJECT](state, object) {
    state.setObject(object);
  },
  /**
   * Mutates the greet in the state
   * @param state
   * @param greet
   */
  [mutationTypes.SET_GREET](state, greet) {
    state.setGreet(greet);
  },
};
