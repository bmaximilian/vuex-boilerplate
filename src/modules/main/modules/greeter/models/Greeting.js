/**
 * Created on 02.08.17.
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import Translator from '@/translator';

class Greeting {

  /**
   * Constructor of Greeting
   * @param greet
   * @param object
   */
  constructor(greet = Translator.t('greeter.greet'), object = '...') {
    this.greet = greet;
    this.object = object;
    this.setText();
  }

  /**
   * Sets the greet of the greeter
   * @param greet
   */
  setGreet(greet) {
    this.greet = greet;
    this.setText();
  }

  /**
   * Sets the object of the greeter
   * @param object
   */
  setObject(object) {
    this.object = object;
    this.setText();
  }

  setText() {
    this.text = this.object === '...' ? `${this.greet}${this.object}` : `${this.greet} ${this.object}`;
  }

}

export default Greeting;
