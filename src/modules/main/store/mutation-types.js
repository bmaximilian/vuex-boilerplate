/**
 * Mutation types of the greeter module
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

export const SET_GREET = 'setGreet';
export const SET_OBJECT = 'setObject';
