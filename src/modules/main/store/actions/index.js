/**
 * Actions og greeter
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import * as actionTypes from '../action-types';
import * as greetActions from '../../modules/greeter/store/action-types';

export default {
  /**
   * Reloads the text of the greeter
   * @param commit
   */
  [actionTypes.RELOAD]({ dispatch }) {
    dispatch(`greeter/${greetActions.RELOAD}`);
  },
};
