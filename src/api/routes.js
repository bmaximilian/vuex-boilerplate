/**
 * Includes the routes to the backend api
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

export const BASE_ROUTE = '//localhost:3000';
export const TEST_GET_ROUTE = `${BASE_ROUTE}/test`;
