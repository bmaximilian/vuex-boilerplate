/**
 * Class to wrap the vue-resource functions
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

class Api {

  /**
   * Function to call a GET API route
   * @param url
   * @param request
   * @param head
   */
  static get(url, request = null, head = {}) {
    return Vue.http.get(url, request, head)
      .catch(error => Promise.reject(error));
  }

  /**
   * Function to call a POST API route
   * @param url
   * @param request
   * @param options
   */
  static post(url, request = null, options = {}) {
    return Vue.http.post(url, request, options)
      .catch(error => Promise.reject(error));
  }

  /**
   * Function to call a PUT API route
   * @param url
   * @param request
   * @param options
   */
  static put(url, request = null, options = {}) {
    return Vue.http.put(url, request, options)
      .catch(error => Promise.reject(error));
  }

  /**
   * Function to call a PATCH API route
   * @param url
   * @param request
   * @param options
   */
  static patch(url, request = null, options = {}) {
    return Vue.http.patch(url, request, options)
      .catch(error => Promise.reject(error));
  }

  /**
   * Function to call a DELETE API route
   * @param url
   * @param request
   */
  static delete(url, request = null) {
    return Vue.http.delete(url, request)
      .catch(error => Promise.reject(error));
  }
}

export default Api;
