/**
 * Loads the Vue-Resource plugin
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */

export * from './routes';
export default from './Api';
